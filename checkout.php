<?php require_once 'core/init.php' ?>
<?php include 'includes/head.php' ?>
<?php include 'val_checkout.php' ?>
<?php include 'all_query.php' ?>

<?php include 'includes/navigate.php' ?>
<style>


    .err{
        color: red;
        font-size: 20px;
    }
</style>




 <div id="myCarousel" class="carousel slide">
   <ol class="carousel-indicators">
     <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
     <li data-target="#myCarousel" data-slide-to="1" ></li>
     <li data-target="#myCarousel" data-slide-to="2" ></li>
     <li data-target="#myCarousel" data-slide-to="3" ></li>
     <li data-target="#myCarousel" data-slide-to="4" ></li>
     <li data-target="#myCarousel" data-slide-to="5" ></li>
     <!-- <li data-target="#myCarousel" data-slide-to="6" ></li> -->
   </ol>

   <div class="carousel-inner">
     <div class="item active">
       <img src="images/home/home1.jpg">
         <div class="container-active">
       <div class="carousel-caption">
         <h1>MOVE ME</h1>
         <p>
           Lorem ipsum dolor sit amet, consectetur adipiscing elit.
           Vestibulum sed consequat tellus, at dapibus dui.
           Duis ultrices, nibh eget faucibus vehicula, velit libero fringilla leo,
           molestie ultrices est mauris ac velit.

         </p>
        <p> <a href="#" class="btn btn-primary">Make Reservation Now</a></p>
       </div>
     </div>
   </div>
   <div class="item">
     <img src="images/home/s.png">
       <div class="container-active">
     <div class="carousel-caption">
       <h1>ALL TRANSPORT COVERED</h1>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit.
         Vestibulum sed consequat tellus, at dapibus dui.
         Duis ultrices, nibh eget faucibus vehicula, velit libero fringilla leo,
         molestie ultrices est mauris ac velit.

       </p>
      <p> <a href="#" class="btn btn-success">Cool Prices For You</a></p>
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/room5.jpg">
       <div class="container-active">
     <div class="carousel-caption">
       <h1>ALL TRANSPORT COVERED</h1>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit.
         Vestibulum sed consequat tellus, at dapibus dui.
         Duis ultrices, nibh eget faucibus vehicula, velit libero fringilla leo,
         molestie ultrices est mauris ac velit.

       </p>
      <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/home/s1.jpeg">
       <div class="container-active" class="fade">
     <div class="carousel-caption">
       <h1>ALL TRANSPORT COVERED</h1>
       <p>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit.
         Vestibulum sed consequat tellus, at dapibus dui.
         Duis ultrices, nibh eget faucibus vehicula, velit libero fringilla leo,
         molestie ultrices est mauris ac velit.

       </p>
      <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>
     </div>
   </div>
 </div>
 <div class="item ">
   <img src="images/meeting/t8.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1>NEW SAFETY MODE</h1>
     <p>
       Lorem ipsum dolor sit amet, consectetur adipiscing elit.
       Vestibulum sed consequat tellus, at dapibus dui.
     </p>
    <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/slider/s444.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1>NEW SAFETY MODE</h1>
     <p>
       Lorem ipsum dolor sit amet, consectetur adipiscing elit.
       Vestibulum sed consequat tellus, at dapibus dui.
     </p>
    <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>
   </div>
 </div>
</div>
 </div>
<a href="#myCarousel" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a href="#myCarousel" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
 <!-- end of carousel -->
</div>
</div>
<!--banner here-->
<?php include 'includes/banner.php';?>

<?php
if(isset($_GET['book'])){
  $id = $_GET['book'];
    $img ="SELECT * FROM rooms WHERE id ='$id'";
    $img_que =$db->query($img);
    $image = mysqli_fetch_assoc($img_que);
 }


?>

<!--first content on page-->
<div class="container-padded">
<div class="checkout justify-content-center">
    <div class="row">
     <div class="col-md-12">
         <h1 class="h1 text-center">Complete Your Reservation</h1><hr><br><br>
         <div class="col-md-8 ">
             <form class="form-horizontal" method="post" action="">
                 <span class="err text-right"><?=$errors ?></span>
                 <span class="err text-right"><?=$errors_1 ?></span>
                 <div class="col-md-4 col-md-offset-2">
                 <label>Firstname*</label>
                 <input type="hidden" value="<?=$image['id']?>" name="id">
                 <input type="text" size="60" class="form-control" name="firstname">  
                 </div>
                 <div class="col-md-4 col-md-offset-1">
                 <label>Lastname*</label>
                 <input type="text" size="60" class="form-control" name="lastname">    
                 </div>
                 <div class="clearfix"></div>
                 
                 <div class="col-md-5 col-md-offset-2">
                 <label>Email*</label>
                 <input type="email" size="60" class="form-control" name="email">      
                 </div>
                 <div class="col-md-3 col-md-offset-1">
                 <label>Telephone*</label>
                 <input type="text" size="60" class="form-control" name="telephone">      
                 </div>
                 <div class="clearfix"></div>
    
                 <div class="col-md-3 col-md-offset-2">
                 <label>Country*</label>
                 <input type="text" size="60" class="form-control" name="country">      
                 </div>
                 <div class="col-md-3 col-md-offset-0.5">
                 <label>City/State*</label>
                 <input type="text" size="60" class="form-control" name="city_state">      
                 </div>
                 <div class="col-md-3 col-md-offset-0">
                 <label>Zip Code</label>
                 <input type="text" size="60" class="form-control" name="zip_code">      
                 </div>
                 <div class="clearfix"></div>
                 
                 <div class="col-md-9 col-md-offset-2">
                 <label>Address</label>
                     <textarea class="form-control" cols="2" rows="8" name="address"></textarea>
                 </div>
                 <div class="clearfix"></div>
                 <br><br><br>
                 
                 <div class="col-md-9 col-md-offset-2">
                 <input type="submit" name="submit" value="Proceed To CheckOut" class="form-control btn btn-lg btn-success">
                 </div>
    
             </form>
         </div>
         <div class="col-md-4">
         <img src="<?=$image['image'] ?>" class="img-responsive img-thumbnail">
         <br>
             <p></p>
             <br>
              <ul>
                <li class="desc">Description :<?=$image['room_type'] ?><span></span></li>
                <li class="amt">Amount :$<?=$image['price'] ?>.00<span></span></li>
              </ul>
         <a href="index.php" class="btn btn-danger">Cancel</a>
         <a href="room.php" class="btn btn-warning">Change Room</a>
         </div>
    </div>
    </div>
</div>
</div>


















<br><br><br>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php include 'includes/footer.php' ?>