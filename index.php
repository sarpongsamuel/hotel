<?php include 'core/init.php' ?>
<?php include 'includes/head.php' ?>
<link href="home.css">
<?php include 'includes/navigate.php' ?>
<?php include 'helpers/helpers.php' ?>

<style type="text/css">
  
      .ttext{
          color: darkseagreen;
          font-family: serif;
      }
    .txt2{
        color: #000;
        padding: 5px;
        font-family: serif;
    }
    .txt{
        color: darkseagreen;
        font-size: 28px;
        font-family: serif;
    }
   .carousel-inner > .item{
  height: 590px;
}
    
    
@media(max-width:468px){
    .myimg{
        height: 200px;
        width: auto;
    }
    
    .carousel-inner > .item{
  height: 400px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 90%; 
}
    .banner{
        font-size: 20px;
        margin-top: -30px;
    }
  }
    
        
@media(max-width:768px){
    .myimg{
        height: 200px;
        width: auto;
    }
    
    #room{
  height: 500px; 
    width: 100%;
        
}
    
    .carousel-inner > .item{
  height: 300px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 20%; 
}
    .banner{
        font-size: 20px;
        margin-top: -20px;
        padding: 20px;
    }
  }
    
    
          
@media(max-width:1126px){
    .myimg{
        height: 200px;
        width: auto;
    }
    
    #room{
  height: 400px; 
    width: 100%;
        
}
    
    .carousel-inner > .item{
  height: 700px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 100%; 
}
    .banner{
        font-size: 30px;
        margin-top: -20px;
        padding: 60px;
        font-family: serif;
    }
  }

            
@media(max-width:1024px){
    
       #room{
  height: 200px; 
    width: 100%;
        
}
    
    .carousel-inner > .item{
  height: 600px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 100%; 
}
    .banner{
        font-size: 10px;
        margin-top: -10px;
    }
  }
    
/*    iphone X*/
    @media(max-width:375px){
    .myimg{
        height: 100px;
        width: 100%;
    }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }
        #room{
            height: 200px;
            width: 100%;
        }
    
    .carousel-inner > .item{
  height: 410px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 5px;
  left: 0;
  min-width: 100%;
  height: 100%; 
}
        
    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
  }
    
    /*    iphone 6/7 */
    @media(max-width:414px){
    .myimg{
        height: 200px;
        width: 100%;
    }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }
        #room{
            height: 230px;
            width: 100%;
        }
        
        .glyphicon-star{
           font-size: 15px;
           color:#009933;
        }
        .glyphicon-user{
          font-size: 15px;
          color: #009933;
       }

       .glyphicon-star-empty{
          font-size: 15px;
          color: #009933;
       }
        .glyphicon-adjust{
          color: #009933;
          font-size: 15px
       }
        .r{
            font-size: 20px;
        }

    
    .carousel-inner > .item{
       height: 410px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%; 
}
        
    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .sel{
            font-size: 30px;
/*            margin-top: -90px;*/
            font-family: serif;
    
        }
  }


</style>

 <div id="myCarousel" class="carousel slide">
<!--
   <ol class="carousel-indicators">
     <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
     <li data-target="#myCarousel" data-slide-to="1" ></li>
     <li data-target="#myCarousel" data-slide-to="2" ></li>
     <li data-target="#myCarousel" data-slide-to="3" ></li>
     <li data-target="#myCarousel" data-slide-to="4" ></li>
     <li data-target="#myCarousel" data-slide-to="5" ></li>
      <li data-target="#myCarousel" data-slide-to="6" ></li> 
   </ol>
-->

   <div class="carousel-inner">
     <div class="item active">
       <img src="images/index/pexels-photo-323780.jpeg">
         <div class="container-active">
       <div class="carousel-caption">
         <h1 class="sel">HOTEL DEMARCIANA</h1>
         <p>welcome To Our Hotel</p>
<!--        <p> <a href="#" class="btn btn-primary">Make Reservation Now</a></p>-->
       </div>
     </div>
   </div>
   <div class="item">
     <img src="images/index/free%20weights.jpg">
       <div class="container-active">
     <div class="carousel-caption">
         <h1 class="sel">HOTEL DEMARCIANA</h1>
         <p>welcome To Our Hotel</p>
<!--      <p> <a href="#" class="btn btn-success">Cool Prices For You</a></p>-->
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/slider/fm9.jpg">
       <div class="container-active">
     <div class="carousel-caption">
  <h1 class="sel">HOTEL DEMARCIANA</h1>
         <p>welcome To Our Hotel</p>
<!--      <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>-->
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/index/spa1.jpg">
       <div class="container-active" class="fade">
     <div class="carousel-caption">
         <h1 class="sel">HOTEL DEMARCIANA</h1>
         <p>welcome To Our Hotel</p>
<!--      <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>-->
     </div>
   </div>
 </div>
 <div class="item ">
   <img src="images/index/bar-KO2.jpg">
     <div class="container-active">
   <div class="carousel-caption">
       <h1 class="sel">HOTEL DEMARCIANA</h1>
         <p>welcome To Our Hotel</p>
<!--    <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>-->
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/slider/s444.jpg">
     <div class="container-active">
   <div class="carousel-caption">
       <h1 class="sel">HOTEL DEMARCIANA</h1>
         <p>welcome To Our Hotel</p>
<!--    <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>-->
   </div>
 </div>
</div>
 </div>
<a href="#myCarousel" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a href="#myCarousel" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
 <!-- end of carousel -->
</div>
</div>
<!--banner here-->
<?php include 'includes/banner.php';?>

<!-- first container on home page -->
<div class="container first_cont">
  <div class="first_cont">
    <div class="row">
      <div class="col-md-12">
        <h1 class="ttext text-center"> Hotel DeMarciana   ~<span class="glyphicon glyphicon-home"></span> </h1><hr class="hr">
        <p class="text1">greets business and leisure travelers with excellent amenities and a perfect location in the heart of Russia's capital city. As soon as your step into our magnificent grand lobby, with its glass dome and fountain, you'll know you've found someplace special. Beautifully designed rooms and suites boast spacious layouts, deluxe pillowtop mattresses, and free Wi-Fi. Many also feature scenic views of Moscow city centre. Additional perks for your hotel visit include a fitness center, a whirlpool, and a indoor pool - plus delicious dining at our two on-site restaurants. Be sure to stop by our vibrant lobby bar for a drink as well, after a busy day in central Moscow. If you're hosting an event here in Russia, we offer 10 flexible venue spaces and expert catering services. And you can stroll to the Kremlin, Red Square, and other city centre attractions in just minutes from our doorstep. Make plans to experience the city in memorable fashion here at the Moscow 
        </p>
      </div>
    </div>
  </div>
</div>

<!-- second container -->
<div class="container-padded">
  <div class="second_cont">
    <div class="container">
      <div class="row">
          <div class="col-md-12">
        <div class="col-md-4">
            <section>
          <h1 class="r">Reviews</h1><br>
          <p>
            <span class="glyphicon glyphicon-adjust"></span>
            <span class="glyphicon glyphicon-adjust"></span>
            <span class="glyphicon glyphicon-adjust"></span>
            <span class="glyphicon glyphicon-adjust"></span>
            <span class="glyphicon glyphicon-adjust"></span>
          </p>
                </section>
        </div>
        <div class="col-md-4">
          <h1 class="r">Viewers</h1><br>
          <p>
            <span class="glyphicon glyphicon-user">1,000,000</span>
          </p>
        </div>
        <div class="col-md-4">
          <h1 class="r">Ratings</h1><br>
          <p>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
          </p>
        </div>
      </div>
    </div>
</div>
</div>
    </div>


<?php
 $rooms = "SELECT * FROM rooms WHERE (deleted ='0' AND available =1) LIMIT 3";
$room_que = $db->query($rooms);
?>

<div class="container">
    <div class="fourth_cont">
    <div class="row">
      <div class="col-md-12">
        <h1 class="text-center ttext">ROOMS AVAILABLE</h1>
        <h3 class="text-center txt2">Relax and be productive in our DeMarciana hotel accommodation</h3>
          <?php while($room =mysqli_fetch_assoc($room_que)): ?>
        <div class="col-md-4"> 
          <section>
            <h3 class="text-center txt"><?=$room['description'] ?></h3><br>
            <img src="<?=$room['image'] ?>" class="img-responsive img-thumbnail" id="room">
            <br>
            <p>
              <br>
              <ul>
                <li class="desc">Room Type: <?=$room['room_type'] ?><span></span></li>
                <li class="amt">Amount :<?=$room['price'] ?><span></span></li>
              </ul>
              </p>
            <a href="checkout.php?book=<?=$room['id'] ?>" class="btn btn-success form-control">Book Now</a>
          </section>
        </div>
        <?php endwhile ?>
      </div>
    </div>
  </div>
  <br>
  <p></p>
  <a href="room.php" class="btn btn-success "><span class="glyphicon glyphicon-plus"></span> View More Rooms</a>
</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php include 'includes/footer.php' ?>