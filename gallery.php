<?php include 'core/init.php' ?>
<?php include 'includes/head.php' ?>
<?php include 'includes/navigate.php' ?>
<style type="text/css">
  .first_cont{
  /*background-color: #91e842*/
}
    .img{
        margin-top: 10px;
    }
    .carousel-inner > .item{
  height: 590px;
}


@media(max-width:468px){
    .myimg{
        height: 200px;
        width: auto;
    }

    .carousel-inner > .item{
  height: 400px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 90%;
}
    .banner{
        font-size: 20px;
        margin-top: -30px;
    }
  }


@media(max-width:768px){
    .myimg{
        height: 200px;
        width: auto;
    }

    #room{
  height: 500px;
    width: 100%;

}

    .carousel-inner > .item{
  height: 300px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 20%;
}
    .banner{
        font-size: 20px;
        margin-top: -20px;
    }
  }



/*    1024 devices*/
@media(max-width:1024px){

    .carousel-inner > .item{
      height: 600px;
     }

    .carousel-inner > .item >img{
        position: relative;
        top: 20px;
        left: 0;
        min-width: 100%;
        height: 100%;
    }
    .banner{
        font-size: 10px;
        margin-top: -10px;
    }

    .gal_head{
        font-size: 50px;
    }

  }




/*  375px   iphone X*/
    @media(max-width:375px){
    .myimg{
        height: 100px;
        width: 100%;
    }
        .htext{
            font-size: 40px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
  height: 410px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
  }

    /*    iphone 6/7 */
    @media(max-width:414px){
        .htext{
            font-size: 30px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
       height: 400px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 40px;
        padding: 25px;
        font-family: serif;
    }
        .carousel-caption{
            font-size: 20px;
        }
        .sel{
            font-size: 30px;
/*            margin-top: -90px;*/
            font-family: serif;

        }
  }
    .gal_head{
        font-size: 40px;
        margin-top: -10px;
    }

    .sty{
        border-radius: 0px;
        color: white;
        border: 1px solid white;
        padding: 5px;
        outline: none;
        background: transparent;
    }

    img1{
        height: 300px
    }
    .conti{
        padding: 20px;
    }
    .room{
        width: 100%;
        height: 300px;
    }

</style>

 <div id="myCarousel" class="carousel slide">
   <div class="carousel-inner">
     <div class="item active">
       <img src="images/bar-KO2.jpg">
         <div class="container-active">
       <div class="carousel-caption">
         <h1 class="sel">HOTEL DEMARCIANA</h1>
           <button class="sty btn btn-success">Enjoy Your Stay</button>
       </div>
     </div>
   </div>
   <div class="item">
     <img src="images/img1.jpg">
       <div class="container-active">
     <div class="carousel-caption">
       <h1 class="sel">HOTEL DEMARCIANA</h1>
           <button class="sty btn btn-success">Nice Rooms Available</button>
<!--      <p> <a href="#" class="btn btn-success">Cool Prices For You</a></p>-->
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/gallery/rm6.jpg">
       <div class="container-active">
     <div class="carousel-caption">
      <h1 class="sel">HOTEL DEMARCIANA</h1>
           <button class="sty btn btn-success">Enjoy Your Stay</button>
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/slide1%20(20).jpg">
       <div class="container-active" class="fade">
     <div class="carousel-caption">
         <h1 class="sel">HOTEL DEMARCIANA</h1>
           <button class="sty btn btn-success">Home Away From Home</button>
     </div>
   </div>
 </div>
 <div class="item ">
   <img src="images/gallery/gym1.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1 class="sel">HOTEL DEMARCIANA</h1>
     <button class="sty btn btn-success">Welcome Home</button>
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/gallery/home_4.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1 class="sel">HOTEL DEMARCIANA</h1>
           <button class="sty btn btn-success">Explore the sights</button>
 </div>
</div>
 </div>
<!-- <a href="#myCarousel" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a href="#myCarousel" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
 <!-- end of carousel -->
</div>
</div>
<?php include 'includes/banner.php' ?>
<br><br>
<!-- first container on home page -->

<div class="container-padded justify-content-center">
<div class="first_container">
<div class="row">
    <div class="col-md-12">
     <h1 class="gal_head  text-center">Hotel At A Glance</h1><hr class="last_hr">
        <div class="container">
        <div class="col-md-12">
<!--         <h3>Now Showing</h3>    -->
        </div>
        </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <div class="col-md-3">
    <img src="images/gallery/gal2.jpeg " class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/g8.jpg'" onmouseout="this.src='images/gallery/gal2.jpeg'">
    </div>
    <div class="col-md-3">
    <img src="images/gallery/gal3.jpeg " class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/g1.jpg'" onmouseout="this.src='images/gallery/gal3.jpeg'">
    </div>
    <div class="col-md-3">
    <img src="images/gallery/gal5.jpeg " class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/g2.jpg'" onmouseout="this.src='images/gallery/gal5.jpeg'">
    </div>
    <div class="col-md-3">
    <img src="images/gallery/gal7.jpeg " class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/go.jpg'" onmouseout="this.src='images/gallery/gal7.jpeg'">
    </div>

    </div>
    </div>
    <div class="clear-fix"></div>
<!--  second row of images  -->
    <br>
    <div class="row">
    <div class="col-md-12">
    <div class="col-md-3">
    <img src="images/gallery/gall13.jpeg " class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/gal3.jpeg'" onmouseout="this.src='images/gallery/gal13.jpg'"></div>
    <div class="col-md-3">
    <img src="images/gallery/gal16.jpeg " class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/gal3.jpeg'" onmouseout="this.src='images/gallery/gal16.jpeg'"></div>
    <div class="col-md-3">
    <img src="images/gallery/night.jpg" class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/rep1.jpg'" onmouseout="this.src='images/gallery/night.jpg'"></div>
    <div class="col-md-3">
    <img src="images/gallery/gal15.jpeg " class="img-responsive img-thumbnails img img1" onmouseover="this.src='images/gallery/rm6.jpg'" onmouseout="this.src='images/gallery/gym3.jpg'">
    </div>
    </div>
    <div class="clear-fix"></div>
</div>
</div>
<br><br>

<!--second container on page-->
<div class="container justify-content-center cont">
 <div class="row">
  <div class="col-md-12">
       <div class="col-md-4 ">
           <a class="btn-block check_in" href="room.php">View  All Rooms  <span class="glyphicon glyphicon-picture"></span></a>
           <br>
        </div>
        <div class="col-md-4 ">
            <a class="btn-block check_out" href="room.php">Make Reservation Now <span class="glyphicon glyphicon-exclamation-sign"></span></a>
            <br>
        </div>
        <div class="col-md-4 ">
            <a class="btn-block book1" href="room.php">View More Rooms <span class="glyphicon glyphicon-eye-open"></span></a>
        <br>
        </div>
</div>
</div>
</div>
<hr>


<!--third container on page-->
<!--
<div class="container-fluid justify-content-center cont1">
 <div class="row">
  <div class="col-md-12">
      <h1 class="gal_head  text-center">Explore the Sights and Sounds</h1><hr class="last_hr">
      <div class="col-md-3">
            <img class="mySlides2 w3-animate-fading img-responsive img" src="images/gallery/gal2.jpeg" >
            <img class="mySlides2 w3-animate-fading img-responsive img" src="images/spa/spa10.jpg" >
            <img class="mySlides2 w3-animate-fading img-responsive img" src="images/home/home2.jpg" >
            <img class="mySlides2 w3-animate-fading img-responsive img-thumbnail img" src="images/chair-cozy-cushion-271648.jpg" >

      </div>

      <div class="col-md-3">
          <div class="w3-content w3-section" style="max-width:500px">
            <img class="mySlides1 w3-animate-top" src="images/spa/spa10.jpg" style="width:100%"><div class="clearfix"></div>
            <img class="mySlides1 w3-animate-bottom" src="images/spa/spa10.jpg" style="width:100%"><div class="clearfix"></div>
            <img class="mySlides1 w3-animate-top" src="images/chair-cozy-cushion-271648.jpg" style="width:100%"><div class="clearfix"></div>
            <img class="mySlides1 w3-animate-bottom" src="images/spa/spa10.jpg" style="width:100%"><div class="clearfix"></div>
          </div>
      </div>
      <div class="col-md-3"><img src="images/chair-cozy-cushion-271648.jpg" class="img-responsive img-thumbnails img">
      <div class="col-md-3"><img src="images/spa/spa10.jpg" class="img-responsive img-thumbnails img">
  </div>
</div>
</div>
    </div>
    </div>
-->
<?php
$rooms ="SELECT * FROM rooms WHERE (deleted=0 AND room_type LIKE '%Presidential Suite%' )LIMIT 4";
$room_que =$db->query($rooms);

?>
<div class="container justify-content-center conti">
 <div class="row">
  <div class="col-md-12">
  <h1 class="gal_head  text-center">Rooms That Will Suite You</h1><hr class="last_hr">
      <?php while($room = mysqli_fetch_assoc($room_que)): ?>
      <div class="col-md-6">
          <img src="<?=$room['image'] ?>" class="img-responsive img-thumbnail img1" >

      </div>
      <div class="col-md-6">
      <h1 class=""><?=$room['room_type'] ?></h1><hr>
          <p>A 109-room boutique hotel ideally located in the historic center of Moscow on the
              pedestrian Arbat Street, famous for its artists, souvenir shops, restaurants, cafés and bars.
              The hotel is very close to two metro stations, the international trade center, Expocentre and the Moscow
              International Business Center. Guests have access to WIFI, tea and coffee
          </p>
          <br>
          <span class="glyphicon glyphicon-heart"></span><span>3.3K</span>
          <span class="glyphicon glyphicon-star pull-right"></span>
          <span class="glyphicon glyphicon-star pull-right"></span>
          <span class="glyphicon glyphicon-star pull-right"></span>
          <span class="glyphicon glyphicon-star pull-right"></span>
          <span class="glyphicon glyphicon-star-empty pull-right"></span>
          <br>
          <a href="checkout.php?book=<?=$room['id'] ?>"><button class="btn btn-success btn pull-right btn-sm">Book Now</button></a>
      </div>

            <?php endwhile ?>

</div>
</div>
</div>


<!--

<div class="container justify-content-center cont">
 <div class="row">
  <div class="col-md-12">
       <form class="book_form1" method="post">
-->
<!--   booking form here   -->
<!--

       <div class="col-md-4 "><a class="btn-block check_in">Check In Date  <span class="glyphicon glyphicon-calendar"></span></a>
           <br>
           <input type="date"  class="form-control hide_d1">
        </div>
        <div class="col-md-4 "><a class="btn-block check_out">Check Out Date <span class="glyphicon glyphicon-calendar"></span></a>
            <br>
        <input type="date" placeholder="" class="form-control hide_d2">
        </div>
-->
<!--
        <div class="col-md-4 "><a class="btn-block book1">Reserve Room Now <span class="glyphicon glyphicon-check"></span></a>
        <br>
        <input type="submit" value="Book Now" class="form-control book_but1">
        </div>
       </form>
</div>
</div>
</div>
-->


<br><br>
<!-- footer here -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php include 'includes/footer.php' ?>
