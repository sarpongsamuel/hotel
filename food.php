<?php include 'includes/head.php' ?>
<?php include 'includes/navigate.php' ?>
<style type="text/css">
  .first_cont{
  /*background-color: #91e842*/
}


@media(max-width:468px){
    .myimg{
        height: 200px;
        width: auto;
    }

    .carousel-inner > .item{
  height: 400px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 90%;
}
    .banner{
        font-size: 20px;
        margin-top: -30px;
    }
  }


@media(max-width:768px){
    .img{
        height: 250px;
        width: 100%;
        margin-top: 10px;
    }

    #room{
  height: 500px;
    width: 100%;

}

    .carousel-inner > .item{
  height: 300px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 20%;
}
    .banner{
        font-size: 20px;
        margin-top: -20px;
    }
  }


@media(max-width:1028px){
    .myimg{
        height: 300px;
        width: 100%;
    }

    .carousel-inner > .item{
  height: 600px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 100%;
}
    .banner{
        font-size: 10px;
        margin-top: -10px;
    }
  }

/*    iphone X*/
    @media(max-width:375px){
    .myimg{
        height: 100px;
        width: 100%;
    }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
  height: 410px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
  }

    /*    iphone 6/7 */
    @media(max-width:414px){
    .myimg{
        height: 200px;
        width: 100%;
    }

        .gal_head{
            font-size: 30px;
            color: darkseagreen;
        }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
       height: 410px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 20px;
        }
  }

        @media(max-width:1126px){
    .myimg{
        height: 200px;
        width: 100%;
    }

        .gal_head{
            font-size: 30px;
            color: darkseagreen;
        }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
       height: 500px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 50px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 30px;
        }
  }

        @media(max-width:360px){
    .img{
        height: 150px;
        width: 100%;
    }

        .gal_head{
            font-size: 30px;
            color: darkseagreen;
        }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
       height: 410px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 20px;
        }
  }
.sty{
        border-radius: 0px;
        color: white;
        border: 1px solid white;
        padding: 5px;
        outline: none;
        background: transparent;
    }


</style>

 <div id="myCarousel" class="carousel slide">
   <div class="carousel-inner">
     <div class="item active">
       <img src="images/food/food4.png">
         <div class="container-active">
       <div class="carousel-caption">
         <h1 class="sel">HOTEL DEMARCIANA</h1>
            <button class="sty btn btn-success">Nice Dishes</button><br>
       </div>
     </div>
   </div>
   <div class="item">
     <img src="images/food/food9.jpg">
       <div class="container-active">
     <div class="carousel-caption">
       <h1 class="sel">HOTEL DEMARCIANA</h1>
            <button class="sty btn btn-success">Welcome Home</button><br>
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/food/food7.jpg">
       <div class="container-active">
     <div class="carousel-caption">
       <h1 class="sel">HOTEL DEMARCIANA</h1>
            <button class="sty btn btn-success">Home Away From Home</button><br>
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/food/food1.jpg">
       <div class="container-active" class="fade">
     <div class="carousel-caption">
    <h1 class="sel">HOTEL DEMARCIANA</h1>
            <button class="sty btn btn-success">Enjoy The Pleasure And Comfort</button><br>
    </div>
   </div>
 </div>
 <div class="item ">
   <img src="images/food/food.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1>HOTEL DEMARCIANA</h1>
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/food/food5.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1>HOTEL DEMARCIANA</h1>

   </div>
 </div>
</div>
 </div>
<a href="#myCarousel" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a href="#myCarousel" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
 <!-- end of carousel -->
</div>
</div>
<?php include 'includes/banner.php';?>
<!-- first container on home page -->
<div class="container ">
  <div class="">
    <div class="row">
      <div class="col-md-4 col1">
        <h2 class="text-center gal_head">Foreign Dishes</h2>
          <p class="text1">
          The restaurant at Hotel DeMarciana offers an intimate yet informal atmosphere where you can dine comfortably with friends,
          family or colleagues and always be sure you will enjoy good quality food. Talented chefs and experienced restaurant teams
          go the extra mile to ensure that your dining experience is a special one.
        </p>
      </div>
        <div class="col-md-8">

            <section>
         <div class="col-md-4">
             <img src="images/food/food8.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food6.png" class="img-responsive img-thumbnail img">
             <img src="images/food/food7.jpg" class="img-responsive img-thumbnail img">
        </div>

         <div class="col-md-4">
             <img src="images/food/food6.png" class="img-responsive img-thumbnail img">
             <img src="images/food/food6.png" class="img-responsive img-thumbnail img">
             <img src="images/food/food1.jpg" class="img-responsive img-thumbnail img">
        </div>

         <div class="col-md-4">
             <img src="images/food/food1.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food10.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food7.jpg" class="img-responsive img-thumbnail img">
        </div>
                <div class="clearfix"></div>

            </section>
        </div>
    </div>
      <br>
      <hr class="hr">
          <div class="row">
      <div class="col-md-4 col1">
        <h2 class="text-center gal_head">Local Dishes</h2>
          <p class="text1">
          The restaurant at  Hotel DeMarciana offers an intimate yet informal atmosphere where you can dine comfortably with friends,
          family or colleagues and always be sure you will enjoy good quality food. Talented chefs and experienced restaurant teams
          go the extra mile to ensure that your dining experience is a special one.
        </p>
      </div>
        <div class="col-md-8">
            <section>
         <div class="col-md-4">
             <img src="images/food/food1.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food10.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food10.jpg" class="img-responsive img-thumbnail img">
        </div>
         <div class="col-md-4">
             <img src="images/food/food1.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food10.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food10.jpg" class="img-responsive img-thumbnail img">
        </div>
         <div class="col-md-4">
             <img src="images/food/food1.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food10.jpg" class="img-responsive img-thumbnail img">
             <img src="images/food/food10.jpg" class="img-responsive img-thumbnail img">
        </div>
            </section>
        </div>
    </div>
  </div>
</div>

<!-- second container -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php include 'includes/footer.php' ?>
