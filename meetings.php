<?php include 'core/init.php' ?>

<?php include 'includes/head.php' ?>

<style>
    .gym{
        height: 400px;
        width: 100%;
    }

.hdtxt{
    color: green;
    font-size: 70px;
    font-family: serif;

}
    .hr{
        width: 50%;
    }

    input[type="text"]{
        border: 1px solid darkseagreen;
        padding: 20px;
        border-radius: 0px;
        height: 30px;
    }

    input[type="submit"]{
        padding-top: 15px;
        border-radius: 0px;
        height: 20px;
        padding-bottom: 30px;
    }


    .err{
        color: red;
        font-size: 20px;
    }
    .toptxt{
        color: lightcoral;
        padding: 5px;
        font-size: 55px;
        font-family: serif;
    }
    .lead{
        color: darkseagreen;
        font-family: serif;
    }
    .myimg{
        height: 400px;
        width: 100%;
    }



@media(max-width:468px){
    .myimg{
        height: 200px;
        width: auto;
    }

    .carousel-inner > .item{
  height: 400px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 90%;
}
    .banner{
        font-size: 20px;
        margin-top: -30px;
    }
  }


@media(max-width:768px){
    .myimg{
        height: 200px;
        width: auto;
    }

    #room{
  height: 500px;
    width: 100%;

}

    .carousel-inner > .item{
  height: 300px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 20%;
}
    .banner{
        font-size: 20px;
        margin-top: -20px;
    }
  }


@media(max-width:1028px){
    .myimg{
        height: 300px;
        width: 100%;
    }

    .carousel-inner > .item{
  height: 600px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 100%;
}
    .banner{
        font-size: 10px;
        margin-top: -10px;
    }
  }

/*    iphone X*/
    @media(max-width:375px){
    .myimg{
        height: 100px;
        width: 100%;
    }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
  height: 410px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
  }

    /*    iphone 6/7 */
    @media(max-width:414px){
    .myimg{
        height: 200px;
        width: 100%;
    }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
       height: 410px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 20px;
        }
  }

     @media(max-width:360px){
    .myimg{
        height: 170px;
        width: 100%;
    }
        .htext{
            font-size: 20px;
        }
        .toptxt{
            font-size: 23px;
        }
         .top{
             font-size: 15px;
         }

    .carousel-inner > .item{
       height: 410px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 20px;
        }
  }







</style>
<?php include 'includes/navigate.php' ?>
<div id="myCarousel" class="carousel slide">
   <!-- <ol class="carousel-indicators">
     <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
     <li data-target="#myCarousel" data-slide-to="1" ></li>
     <li data-target="#myCarousel" data-slide-to="2" ></li>
     <li data-target="#myCarousel" data-slide-to="3" ></li>
     <li data-target="#myCarousel" data-slide-to="4" ></li>
     <li data-target="#myCarousel" data-slide-to="5" ></li>
     <li data-target="#myCarousel" data-slide-to="6" ></li>
     <li data-target="#myCarousel" data-slide-to="6" ></li>

   </ol> -->

   <div class="carousel-inner">
     <div class="item active">
       <img src="images/meeting/t1.jpg">
         <div class="container-active">
       <div class="carousel-caption">
       </div>
     </div>
   </div>
   <div class="item">
     <img src="images/meeting/t8.jpg">
       <div class="container-active">
     <div class="carousel-caption">
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/meeting/t2.jpg">
       <div class="container-active">
     <div class="carousel-caption">
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/meeting/t8.jpg">
       <div class="container-active">
     <div class="carousel-caption">
     </div>
   </div>
 </div>
 <div class="item ">
   <img src="images/meeting/t3.jpg">
     <div class="container-active">
   <div class="carousel-caption">
   </div>
 </div>
</div>
 <div class="item ">
   <img src="images/meeting/t1.jpg">
     <div class="container-active">
   <div class="carousel-caption">
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/meeting/c2.jpg">
     <div class="container-active">
   <div class="carousel-caption">
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/meeting/t9.jpg">
     <div class="container-active">
   <div class="carousel-caption">
   </div>
 </div>
</div>
 </div>
 <!-- end of carousel -->
</div>
<!--banner goes here-->
<?php include 'includes/banner.php';?>

<div class="container">
<div class="pics">
<div class="row">
<div class="col-md-6">
<img src="images/meeting/c1.jpg" class="img-responsive" alt="">
</div>
<div class="col-md-6">
  <h1 class="text-left">Conference Hall I</h1>
  <p class="lead">
    You need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined
    You need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined
    You need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined
  </p>
</div>
</div>
<div class="clearfix"></div>
<br><hr>
<div class="row">
<div class="col-md-6">
  <h1 class="text-left">Conference Hall II</h1>
  <p class="lead">
    You need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined
    You need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined
    You need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined
  </p>
</div>
<div class="col-md-6">
<img src="images/meeting/c6.jpg" class="img-responsive" alt="">
</div>
</div>
</div>

</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php include 'includes/footer.php' ?>
