-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2019 at 02:20 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marciana`
--

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone` int(11) NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `room_number` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `checkout`
--

INSERT INTO `checkout` (`id`, `firstname`, `lastname`, `email`, `telephone`, `country`, `city_state`, `zip_code`, `address`, `room_number`, `deleted`) VALUES
(17, 'Awuah', 'Samuel', 'sarps@gmail.com', 501592514, 'Ghana', 'Koforidua', 233, 'Te 277', 17, 0);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `email`, `message`) VALUES
(1, 'goldbergjaguar@gmail.com', 'helo');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `visible`) VALUES
(1, '/atom/hotel/images/gallery/g12.jpg', 0),
(2, '/atom/hotel/images/gallery/g12.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gym`
--

CREATE TABLE `gym` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gym`
--

INSERT INTO `gym` (`id`, `firstname`, `email`, `duration`, `room`) VALUES
(1, 'AWUAH SAMUEL', 'goldbergjaguar@gmail.com', '3 months', '1');

-- --------------------------------------------------------

--
-- Table structure for table `gym_image`
--

CREATE TABLE `gym_image` (
  `id` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gym_image`
--

INSERT INTO `gym_image` (`id`, `image`, `deleted`) VALUES
(1, '/atom/hotel/images/gallery/g10.jpg', 0),
(2, '/atom/hotel/images/gym/p1.jpg', 0),
(3, '/atom/hotel/images/gym/ten1.jpg', 0),
(4, '/atom/hotel/images/gallery/g10.jpg', 0),
(5, '/atom/hotel/images/gym/gym4.jpg', 0),
(6, '/atom/hotel/images/gym/gym1.jpg', 0),
(7, '/atom/hotel/images/gym/ten3.jpg', 0),
(8, '/atom/hotel/images/gym/gym7.jpg', 0),
(9, '/atom/hotel/images/gym/t1.jpg', 0),
(10, '/atom/hotel/images/gym/gym5.jpg', 0),
(11, '/atom/hotel/images/gym/v1.png', 0),
(12, '/atom/hotel/images/gym/gym3.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image_url` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--

CREATE TABLE `meeting` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `manager` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `secure` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `expiry` date NOT NULL,
  `purpose` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meeting_image`
--

CREATE TABLE `meeting_image` (
  `id` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `head_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `sub_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `available` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `meeting_image`
--

INSERT INTO `meeting_image` (`id`, `image`, `head_text`, `deleted`, `sub_text`, `price`, `available`) VALUES
(1, '/atom/hotel/images/meeting/t9.jpg', 'Marciana Auditorium', 0, 'A 5000 Capacitor Hall', 2000, 1),
(2, '/atom/hotel/images/meeting/c6.jpg', 'Bernadette Auditorium', 0, 'A 3000 Capacitor Hall', 1500, 1),
(3, '/atom/hotel/images/meeting/t7.jpg', 'Killian Auditorium', 0, 'A 2000 Capacitor Hall', 1000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `in_date` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `out_date` date NOT NULL,
  `acc_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `acc_number` int(11) NOT NULL,
  `card_number` int(100) NOT NULL,
  `card_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `email`, `in_date`, `out_date`, `acc_name`, `acc_number`, `card_number`, `card_type`, `deleted`) VALUES
(1, 'website@gmail.com', '2018-08-02', '2018-08-04', 'heloo', 2147483647, 0, 'Master Card', 0),
(2, 'shaunta@gmail.com', '2018-08-01', '2018-08-09', 'samuel sarpong', 2147483647, 0, 'PayPal', 0),
(3, 'login@gmail.com', '2018-08-11', '2018-08-11', 'samuel sarpong', 2147483647, 0, 'Master Card', 0),
(4, 'goldbergjaguar@gmail.com', '2018-08-16', '2018-08-24', 'samuel sarpong', 0, 0, 'PayPal', 0),
(5, 'goldbergjaguar@gmail.com', '2018-08-17', '2018-08-18', 'samuel sarpong', 125147896, 0, 'Master Card', 0),
(6, 'shaunta@gmail.com', '2018-09-19', '2018-09-20', 'samuel sarpong', 2147483647, 0, 'Master Card', 0),
(7, 'sarps@gmail.com', '2018-11-02', '2018-11-30', 'samuel sarpong', 2147483647, 0, 'Master Card', 0),
(8, 'goldbergjaguar@gmail.com', '2018-11-13', '2018-11-13', 'samuel sarpong', 125147, 0, 'E-Zwich', 0),
(9, 'shaunta@gmail.com', '0000-00-00', '2018-11-02', 'samuel sarpong', 2022000, 0, 'Master Card', 0),
(10, 'shaunta@gmail.com', '2018-11-14', '2018-11-16', 'samuel sarpong', 2147483647, 0, 'Master Card', 0),
(11, 'sarps@gmail.com', '2018-12-24', '2018-12-26', 'samuel sarpong', 2147483647, 1234, 'Master Card', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `images` text COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `images`, `name`, `message`, `time`, `deleted`) VALUES
(4, '/atom/hotel/images/icon.png', 'elorm', 'sammy, I love this website', '2018-11-14 04:45:20', 1),
(5, '/atom/hotel/images/user.jpg', 'eben', 'awesome', '2018-08-10 17:37:22', 1),
(6, '/atom/hotel/images/user.jpg', 'Samuel ', 'we love you ', '2018-08-20 16:14:38', 1),
(7, '/atom/hotel/images/icon.png', 'Samuel ', 'we love you ', '2018-11-14 03:01:50', 0),
(8, '/atom/hotel/images/icon.png\r\n', 'samuel sarpong', 'nice website', '2018-11-14 03:01:30', 0),
(9, '/atom/hotel/images/icon.png', 'Sarps ', 'I trust this is nice ', '2018-11-14 03:00:58', 0),
(10, '/atom/hotel/images/icon.png', 'socializing18', 'test img', '2018-11-14 04:45:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `room_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `available` int(11) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_type`, `room_size`, `description`, `price`, `image`, `available`, `deleted`) VALUES
(1, 'Queen Suite', '300x500', 'awesome', 700, '/atom/hotel/images/room/bed6.jpg', 1, 0),
(2, 'Junior Suite', '400x566', 'nice apartment', 500, '/atom/hotel/images/room/bed2.jpg', 1, 0),
(3, 'single room', '400x566', 'cool', 300, '/atom/hotel/images/room/bed5.jpeg', 1, 0),
(4, 'family size', '400x566', 'nice', 500, '/atom/hotel/images/room/bed2.jpg', 1, 0),
(5, '300X400', '', 'Junior Suite', 300, '/atom/hotel/images/room/bed7.jpg', 1, 0),
(6, '300X400', '', 'Junior Suite', 300, '/atom/hotel/images/room/bed7.jpg', 1, 0),
(7, 'junior suite', '400x566', 'nice', 500, '/atom/hotel/images/room/bed14.jpg', 1, 0),
(8, 'family size', '400x566', 'nice', 26, '/atom/hotel/images/room/bed10.jpg', 1, 0),
(9, 'junior suite', '400x566', 'cool', 26, '/atom/hotel/images/room/bed3.jpg', 1, 0),
(10, 'junior suite', '400x566', 'awesome', 24, '/atom/hotel/images/room/pexels-photo-271805.jpeg', 1, 0),
(11, 'junior suite', '400x566', 'kokok', 24, '/atom/hotel/images/room/bed21.jpg', 1, 0),
(12, 'Presidential Suite', '400x566', 'cool2', 400, '/atom/hotel/images/room/bed7.jpg', 1, 0),
(13, 'junior suite', '400x566', 'welcome', 24, '/atom/hotel/images/room/bed4.jpg', 1, 0),
(14, 'Presidential Suite', '400x566', 'awesome', 33, '/atom/hotel/images/room/bed17.jpg', 1, 0),
(15, 'samuel', '700x200', 'carousel', 20, '/atom/hotel/images/room/bed11.jpg', 1, 0),
(16, 'classic', '400x566', 'queen', 79, '/atom/hotel/images/room/bed17.jpg', 1, 0),
(17, 'classic', '400x566', 'classic', 33, '/atom/hotel/images/room/bed22.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `spa`
--

CREATE TABLE `spa` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `room` int(11) NOT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `spa`
--

INSERT INTO `spa` (`id`, `firstname`, `email`, `room`, `duration`, `deleted`) VALUES
(1, 'EDMUND', 'goldbergjaguar@gmail.com', 0, '3', 1),
(2, 'samuel sarpong', 'sarpongawuahfee@gmail.com', 23, '3', 1),
(3, 'smaila', 'goldbergjaguar@gmail.com', 14, '3 weeks', 1),
(4, 'alex', 'goldbergjaguar@gmail.com', 2, '3 months', 1),
(5, 'somuah', 'shantel@gmail.com', 17, '2 weeks', 1),
(6, 'sam', 'shantel@gmail.com', 17, '2 weeks', 1),
(7, 'samuel', 'shantel@gmail.com', 17, '2 weeks', 1),
(8, 'samuel', 'goldbergjaguar@gmail.com', 1, '3 months', 1),
(9, 'samuel', 'goldbergjaguar@gmail.com', 1, '3 months', 1);

-- --------------------------------------------------------

--
-- Table structure for table `spa_images`
--

CREATE TABLE `spa_images` (
  `id` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `spa_images`
--

INSERT INTO `spa_images` (`id`, `image`, `deleted`) VALUES
(1, '/atom/hotel/images/spa/spa1.jpg', 0),
(2, '/atom/hotel/images/spa/spa15.jpg', 0),
(3, '/atom/hotel/images/spa/spa9.jpg', 0),
(4, '/atom/hotel/images/spa/spa13.jpg', 0),
(5, '/atom/hotel/images/spa/spa6.jpg', 0),
(6, '/atom/hotel/images/spa/spa8.jpg', 0),
(7, '/atom/hotel/images/spa/spa14.jpg', 0),
(8, '/atom/hotel/images/spa/spa2.jpg', 0),
(9, '/atom/hotel/images/spa/spa5.jpg', 0),
(10, '/atom/hotel/images/spa/spa4.jpg', 0),
(11, '/atom/hotel/images/spa/spa12.jpg', 0),
(12, '/atom/hotel/images/spa/spa3.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gym`
--
ALTER TABLE `gym`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gym_image`
--
ALTER TABLE `gym_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meeting`
--
ALTER TABLE `meeting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meeting_image`
--
ALTER TABLE `meeting_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spa`
--
ALTER TABLE `spa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spa_images`
--
ALTER TABLE `spa_images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gym`
--
ALTER TABLE `gym`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gym_image`
--
ALTER TABLE `gym_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meeting`
--
ALTER TABLE `meeting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meeting_image`
--
ALTER TABLE `meeting_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `spa`
--
ALTER TABLE `spa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `spa_images`
--
ALTER TABLE `spa_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
