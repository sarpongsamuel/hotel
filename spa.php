<?php include 'core/init.php' ?>
<?php include 'ver_spa.php' ?>

<?php include 'includes/head.php' ?>
<style>
.hdtxt{
    color: green;
    font-size: 70px;
    font-family: serif;

}
    .hr{
        width: 50%;
    }

    input[type="text"]{
        border: 1px solid darkseagreen;
        padding: 20px;
        border-radius: 0px;
        height: 30px;
    }

    input[type="submit"]{
        padding-top: 15px;
        border-radius: 0px;
        height: 20px;
        padding-bottom: 30px;
    }
    .banner1{
        padding: 25px;
        color: white;
        background-color: #e34234;
    }

    .err{
        color: red;
        font-size: 20px;
    }
     @media(max-width:768px){
    #room{
        height: 300px;
        width: 100%;
    }
        .htext{
            font-size: 30px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
       height: 400px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 20px;
        }
  }

     @media(max-width:1024px){
    #room{
        height: 300px;
        width: 100%;
    }
        .htext{
            font-size: 30px;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
       height: 410px;
    }

    .banner{
      margin-top: -5%;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}
        .carousel-caption h1{
            margin-top: -10%;
            font-size: 20px;
        }
  }


    @media(max-width:375px){
    .carousel-inner > .item{
       height: 460px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 15px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

        .htext{
            font-size: 25px;
        }

        .carousel-caption{
            font-size: 30px;
        }
    #room{
        width: 100%;
        padding: 5px;
        height: 100px;
    }
        .hdtxt{
         font-size: 40px;
            font-family: serif;
        }
        .hr{
            width: 100%;

        }
        #room{
            width: 100%;
            height: 200px;
        }
        .banner1{
            font-size: 20px;
        }
  }
    .carousel-inner > .item{
  height: 590px;
}

    @media(max-width:414px){
        .carousel-caption{
            font-size: 30px;
        }
    #room{
        width: 100%;
        padding: 5px;
        height: 220px;
    }
        .htext{
            font-size: 20px;
        }
        .hdtxt{
            font-size: 30px;
        }
  }
</style>
<?php include 'includes/navigate.php' ?>

 <div id="myCarousel" class="carousel slide">
<!--
   <ol class="carousel-indicators">
     <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
     <li data-target="#myCarousel" data-slide-to="1" ></li>
     <li data-target="#myCarousel" data-slide-to="2" ></li>

   </ol>
-->

   <div class="carousel-inner">
     <div class="item active">
       <img src="images/spa/spa1.jpg">
         <div class="container-active">
         </div>
       <div class="carousel-caption">
     </div>
   </div>
   <div class="item">
     <img src="images/spa/spa2.jpg">
       <div class="container-active">
     <div class="carousel-caption">

     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/spa/spa12.jpg">
       <div class="container-active">
     <div class="carousel-caption">

     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/spa/spa11.jpg">
       <div class="container-active" class="fade">
     <div class="carousel-caption">

     </div>
   </div>
 </div>
 <div class="item ">
   <img src="images/spa/spa15.jpg">
     <div class="container-active">
   <div class="carousel-caption">
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/spa/spa6.jpg">
     <div class="container-active">
   <div class="carousel-caption">
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/spa/spa13.jpg">
     <div class="container-active">
   <div class="carousel-caption">
   </div>
 </div>
</div>
  </div>
<!-- <a href="#myCarousel" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a href="#myCarousel" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
 <!-- end of carousel -->
</div>
<!--banner here-->
<div class="container-padded">
  <?php include 'includes/ban.php'; ?>
<div class="pics">
<div class="row">
<div class="col-md-12">
<h1 class="hdtxt text-center">Hotel Spa</h1> <hr class="hr"><br>
    <div class="col-md-12">
        <?php
    $img ="SELECT * FROM spa_images WHERE deleted =0";
    $img =$db->query($img);

    ?>
    <div class="col-md-9">
    <div class="row">
        <h1 class="txt text-center">ALL Images</h1><br>
      <?php while($rooms=mysqli_fetch_assoc($img)) :?>
      <div class="col-md-3">
        <img src="<?php echo $rooms['image']; ?>" alt="" class="img-responsive img-thumbnails" id="room">
        <br>
        </div>
      <?php endwhile; ?>
      </div>
                <div class="clearfix"></div><br>

    </div>
  <div class="colme">
<div class="col-md-3">
    <h3 class="txt">Spa Reservation</h3><hr>
  <span class="err text-right"><?=$errors ?></span>
<span class="err text-right"><?=$errors_1 ?></span>
        <form method="post" action="">
        <input type="text" class="form-control" placeholder="Enter name here" name="firstname">
        <br>

        <input type="text" class="form-control" placeholder="Email" name="email">
        <br>

        <input type="text" class="form-control" placeholder="room ID" name="room">
        <br>


        <input type="text" class="form-control" placeholder="training duration" name="duration">
        <br>


        <input type="submit" name="submit" value="Register" class="form-control btn btn-success">
        <br>

        <br>

        </form>
</div>
    </div>

<!--second row of images-->
</div>
</div>
</div>
</div>

</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php include 'includes/footer.php' ?>
