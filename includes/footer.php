<div class="container-padded">
  <div class="last_cont">
    <div class="row">
      <div class="container">
        <h4><span class="glyphicon glyphicon-plus"></span> Top Destinations</h4>
        <hr class="hr_last">
        <h6><span>
          <ul class="last">
            <li><a href=""> Help</a></li>
            <li><a href=""> Visit Us</a></li>
            <li><a href=""> View Licence</a></li>
            <li><a href=""> Privacy Center</a></li>
          </ul>
        </span></h6>
        <br>
        <br>
        <br>
        <p>
          <br>
          Best Available Rate Guarantee assures you receive the best rates when you book directly with us. If you find a lower publicly available rate within 24 hours of booking, we will match that rate plus give you 25% off the lower rate, subject to guarantee terms and exclusions. Guarantee does not apply to Ritz-Carlton Montreal, The Ritz London, Ritz-Carlton Residences®, and Starwood-Branded Hotels, including Four Points Hotels, Sheraton Hotels, Aloft Hotels, W Hotels, Le Meridien Hotels, Luxury Collection Hotels, Element Hotels, Westin Hotels, St. Regis Hotels, Tribute Portfolio Hotels and Design Hotels. Marriott Rewards® and The Ritz-Carlton Rewards® members (“Rewards Members”) who book rooms through a Marriott® Direct Booking Channel, authorized travel agents or select corporate travel partners ("Eligible Channels") at hotels that participate in Marriott Rewards® and The Ritz-Carlton Rewards loyalty programs will receive an exclusive, preferred rate (“Marriott Rewards Member Rate”). Member Rates are available globally at all hotels that participate in Marriott Rewards Exclusions apply. See our Terms & Conditions for additional details related to our Best Available Rate Guarantee and Marriott Rewards Member Rate. Hotels shown on Marriott.com may be operated under a license from Marriott International, Inc. or one of its affiliates.
        </p>
        <br>
        <div class="footer">Copyright@2018</div>
      </div>
    </div>
  </div>
</div>

<script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>

<!--
<script type="text/javascript">
    function autoRefreshPage()
    {
        window.location = window.location;
    }
    setInterval('autoRefreshPage()', 30000);
</script>
-->

    <script>
      $(function(){
         $('.carousel').carousel('cycle')

    });
    </script>

<script>
$(document).ready(function(){
 $("input.hide_d1").hide();
 $("input.hide_d2").hide();
 $("input.book_but1").hide();
    
 $("a.check_out").click(function(){
     $("input.hide_d2").fadeToggle("slow");
 });

    
 $("a.check_in").click(function(){
     $("input.hide_d1").fadeToggle("slow");
 });

     $("a.book1").click(function(){
//         alert("hello")
     $("input.book_but1").fadeToggle("slow");
 });

});
</script>

<!--fade images-->
<script>
var myIndex = 0;
carousel2();

function carousel2() {
    var i;
    var x = document.getElementsByClassName("mySlides2");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 9000);    
}
    var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides1");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2500);    
}
</script>
  </body>
</html>