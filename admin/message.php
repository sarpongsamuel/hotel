<?php include '../core/init.php'?>
<?php session_start()?>
<?php
 if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
  header('Location: login.php');
}
?>


<?php include 'includes/head.php'?>
<style>
    .big_but{
        padding: 40px;
        border-radius: 10px;
        font-size: 20px;
    }

    body{
/*        background-image: image('../images/rm2.jpg')*/
    }

    strong{
        font-family: sans-serif;
        font-size: 20px;
        color: darkseagreen;
    }

    .glyphicon-comment{
    color: darkseagreen;
    font-size: 15px;
}
</style>

<?php
    $msg ="SELECT * FROM reviews WHERE deleted =0";
    $msg_que =$db->query($msg);

?>


<?php
if(isset($_GET['delete'])){
    $del_id =$_GET['delete'];
    $del_id -(int)$del_id;

    $up ="UPDATE reviews SET `deleted` ='1' WHERE `id` ='$del_id'";
    $db->query($up);
    header('Location: index.php');

}

?>
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
      <div class="container">
      <div class="home">
          <h1 class="text-right">::: Message Logs</h1><hr class="hr">
      <div class="row">
       <div class="col-md-12">
        <section class="rev">
        <?php while($msgs =mysqli_fetch_assoc($msg_que)) :?>
        <div class="media">
          <a href="#" class="pull-left">
            <img src="<?=$msgs['images'] ?>" class="media-object" alt="user">
          </a>
            <a href="message.php?delete=<?=$msgs['id'] ?>" class=" btn btn-danger pull-right btn-sm"> <span class="glyphicon glyphicon-trash"></span> </a>
          <div class="media-body">
            <strong><h4 class="media-heading ">@<?=$msgs['name'] ?></h4></strong>
            <p class="bpad "><span class="glyphicon glyphicon-comment"></span> <?=$msgs['message'] ?></p>
              <br>
          </div>
        </div>
            <br>
            <hr class="hr">
        <?php endwhile; ?>
      </section>

     </div>
      </div>
</div>
</div>

      <?php include 'includes/footer.php' ?>
