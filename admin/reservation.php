<?php include '../core/init.php'?>
<?php session_start()?>
<?php
 if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
  header('Location: login.php');
}
?>
<?php include 'includes/head.php'?>
<style>
    .big_but{
        padding: 40px;
        border-radius: 10px;
        font-size: 20px;
    }

    body{
/*        background-image: image('../images/rm2.jpg')*/
    }
</style>

<?php
    $client ="SELECT * FROM checkout";
    $client_que =$db->query($client);
?>


<!-- delete clients reservation details fro our database  -->
<?php
if(isset($_GET['delete'])){
    $del_id =$_GET['delete'];


    $del ="DELETE FROM checkout WHERE id ='$del_id' AND deleted ='1'";
    $db->query($del);
    header('Location: reservation.php');
}
?>

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
      <div class="container">
      <div class="home">
          <h1 class="text-right">::: All Reservations </h1><hr class="hr">
      <div class="row">
       <div class="col-md-12">
           <div class="table-responsive">
           <table class="table">
           <thead>
               <th>id</th>
               <th>Firstname</th>
               <th>Lastname</th>
               <th>Email</th>
               <th>Telephone</th>
               <th>Country</th>
               <th>City/State</th>
               <th>Address</th>
               <th>Zip Code</th>
               <th>Room #</th>
               <th>Book Status</th>
               <th>Action</th>
           </thead>
               <tbody>
                   <?php while($clients = mysqli_fetch_assoc($client_que)) :?>
               <tr>
                   <td><?=$clients['id'] ?></td>
                   <td><?=$clients['firstname'] ?></td>
                   <td><?=$clients['lastname'] ?></td>
                   <td><?=$clients['email'] ?></td>
                   <td><?=$clients['telephone'] ?></td>
                   <td><?=$clients['country'] ?></td>
                   <td><?=$clients['city_state'] ?></td>
                   <td><?=$clients['address'] ?></td>
                   <td><?=$clients['zip_code'] ?></td>
                   <td><?=$clients['room_number'] ?></td>
                   <td><a href="#" class="btn btn-<?=(($clients['deleted'] == '0')? 'success': 'warning') ?> btn-sm"><?=(($clients['deleted'] == '0')? 'available': 'Not available') ?></a></td>
          <?php if($clients['deleted'] == '1'){ ?>
            <td> <a href="reservation.php?delete=<?=$clients['id']; ?>" class="btn btn-danger">Del</a> </td>
          <?php }else{ ?>
                 <td> <a href="#" class="btn btn-default">N/A</a></td>
           <?php } ?>
               </tr>
               </tbody>
               <?php endwhile ?>
           </table>
           </div>

     </div>
      </div>
</div>
</div>

      <?php include 'includes/footer.php' ?>
