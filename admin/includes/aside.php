<?php require_once '../core/init.php' ?>
<?php include '../all_query.php' ?>
<aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>hotel deMarciana</p>
          <a href="#"><i class="fa fa-circle text-success"></i>Administrator</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <ul class="sidebar-menu">
        <li class="header">Hotel Management</li>
        <li class="treeview">
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li>
          <!-- get total number of booked rooms -->
          <?php
          $count_booked_room = 'SELECT * FROM rooms WHERE available="0" and deleted="0"';
          $count_booked_room_query = $db->query($count_booked_room);
          $count_booked = mysqli_num_rows($count_booked_room_query);
          ?>
          <a href="booked.php">
            <i class="fa fa-th"></i> <span>Booked Rooms</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"><?=$count_booked; ?></small>
            </span>
          </a>
        </li>
        <li>
          <!-- get total number of available rooms -->
          <?php
          $count_avail_room = 'SELECT * FROM rooms WHERE available="1" and deleted="0"';
          $count_avail_room_query = $db->query($count_avail_room);
          $count_avail = mysqli_num_rows($count_avail_room_query);
          ?>
          <a href="available.php">
            <i class="fa fa-th"></i> <span>Available Rooms</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?=$count_avail; ?></small>
            </span>
          </a>
        </li>
        <li>
          <!-- get clients commemts -->
          <?php
          $comments_query = 'SELECT * FROM reviews WHERE deleted="0"';
          $comment = $db->query($comments_query);
          $count_comment = mysqli_num_rows($comment);
          ?>

          <a href="message.php">
            <i class="fa fa-comment"></i> <span>Comments</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-blue"><?=$count_comment ?></small>
            </span>
          </a>
        </li>
        <!-- <li class="treeview">
          <a href="accounts.php">
            <i class="fa fa-folder"></i> <span>Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right  glyphicon glyphicon-wrench "></i>
            </span>
          </a>

        </li> -->
        </ul>
    </section>
  </aside>
