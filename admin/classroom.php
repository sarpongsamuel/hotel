<?php
include_once 'head.php';
include_once 'header.php';
include_once 'sidebar.php';
// include_once 'last.php';
?>

<div class="content-wrapper">
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><b>Class Room</b> <small>preview</small></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <form role="form" action="index.php" method="post">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="enter name">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Student Count</label>
              <input type="text" name="count" class="form-control" id="exampleInputPassword1" placeholder="student count">
            </div>

            <div class="form-group">
              <label for="exampleInputPassword1">Hall Charge</label>
              <input type="text" name="charge" class="form-control" id="exampleInputPassword1" placeholder="hall charge">
            </div>


            </div>

          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>

      </div>
    </div>
  </div>
</section>

<!-- Main content -->

<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>


</div><!-- /.content-wrapper -->

<?php include_once 'footer.php'; ?>
