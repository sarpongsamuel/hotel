<?php include '../core/init.php'?>
<?php session_start()?>
<?php
 if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
  header('Location: login.php');
}
?>
<?php include 'includes/head.php'?>
<style>
    .big_but{
        padding: 40px;
        border-radius: 10px;
        font-size: 20px;
    }
    
    body{
/*        background-image: image('../images/rm2.jpg')*/
    }
</style>

<?php
    $client ="SELECT * FROM spa WHERE deleted ='0'";
    $client_que =$db->query($client);  
?>


<?php
if(isset($_GET['delete'])){
    $del_id =$_GET['delete'];
    $del_id -(int)$del_id;
    
    $up ="UPDATE spa SET `deleted` ='1' WHERE `id` ='$del_id'";
    $db->query($up);
        header('Location: spa.php');

}

?>
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>  
      <div class="container">
      <div class="home">
          <h1 class="text-right">::: Spa Reservations</h1><hr class="hr">
      <div class="row">
       <div class="col-md-12">
           <div class="table-responsive">
           <table class="table table-striped table-bordered table-condensed">
           <thead>
               <th>Delete</th>
                <th>id</th>
               <th>Fullname</th>
               <th>Email</th>
               <th>Room Number</th>
               <th>Duration</th>
           </thead>
               <tbody>
                   <?php while($clients = mysqli_fetch_assoc($client_que)) :?>
               <tr>
                   <td><a href="spa.php?delete=<?=$clients['id'] ?>" class="btn btn-danger btn-sm">delete <span class="glyphicon glyphicon-trash"></span></a></td>
                    <td><?=$clients['id'] ?></td>
                   <td><?=$clients['firstname'] ?></td>
                   <td><?=$clients['email'] ?></td>
                   <td><?=$clients['room'] ?></td>
                   <td><?=$clients['duration'] ?></td>
               </tr>
               </tbody>
               <?php endwhile ?>
           </table>
           </div>
        
     </div>   
      </div> 
</div>
</div>

      <?php include 'includes/footer.php' ?>

