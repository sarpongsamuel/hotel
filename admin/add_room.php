<?php require_once '../core/init.php' ?>
<?php session_start()?>
<?php
 if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
  header('Location: login.php');
}
?>
<?php include 'includes/head.php'?>
<?php include 'new_room.php'?>


<style>
    .big_but{
        padding: 40px;
        border-radius: 10px;
        font-size: 20px;
    }

    .err{
        color: red;
        font-size: 20px;
    }



</style>

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
      <div class="container">
      <div class="home">
          <h1 class="text-right">Add New Room</h1><hr class="hr">
            <div class="row">
                 <a href="index.php"><button class="btn btn-md btn-warning pull-right"> Go Home </button></a>
                <div class="col-md-12">
                    <br><br>
                    <?=$errors ?>
                 <form method="post" action="">
                 <div class="col-md-6 col-md-offset-2">
                 <label>Room Type*</label>
                     <select class="form-control" name="type">
                      <option value="Presidential Suite">Presidential Suite</option>
                      <option value="Junior Suite">Junior Suite</option>
                      <option value="Queen Suite">Queen Suite</option>
                      <option value="Common Room">Common Room</option>
                      <option value="Family Size">Family Size</option>
                     </select>
                 </div>
                 <div class="col-md-3 col-md-offset-1">
                 <label>Price*</label>
                 <input type="text" size="60" class="form-control" name="price">
                 </div>
                 <div class="clearfix"></div>

                 <div class="col-md-4 col-md-offset-2">
                 <label>Room Size*</label>
                 <input type="text" size="60" class="form-control" name="room_size">
                 </div>
                 <div class="col-md-4 col-md-offset-1">
                 <label>Select Room Image*</label>
                  <input type="file" class="form-control" name="photo">

                 </div>
                 <div class="clearfix"></div>

                 <div class="col-md-10 col-md-offset-2">
                 <label>Description*</label>
                 <textarea class="form-control" cols="3" rows="6" name="description"></textarea>
                 </div>
                 <div class="clearfix"></div>
                 <br><br><br>
                 <div class="col-md-10 col-md-offset-2">
                 <input type="submit" name="submit" value="Add New Room" class="btn btn-primary form-control">
                 </div>
                 <div class="clearfix"></div>
                 </form>
                </div>
      </div>
     </div>

      </div>


</div>

      <?php include 'includes/footer.php' ?>
