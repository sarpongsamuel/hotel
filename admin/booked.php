<?php include '../core/init.php'; ?>
<?php session_start()?>
<?php
 if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
  header('Location: login.php');
}
?>
<?php include 'includes/head.php'?>
<?php
 $rooms = "SELECT * FROM rooms WHERE (deleted ='0' AND available ='0')";
$room_que_b = $db->query($rooms);
?>


<style>
    .big_but{
        padding: 40px;
/*        border-radius: 10px;*/
        font-size: 20px;
        margin-top: 10px;
    }
    #btn_free{
        background-color: #a6f1a6;
        color: black;
    }
    
    #btn_full{
        background-color:#f5bcd9;
        color: black;
    }
    
    body{
/*        background-image: image('../images/rm2.jpg')*/
    }
</style>
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>  
      <div class="container">
      <div class="home">
          <h1 class="text-right">Booked Room</h1><hr class="hr">
           <br><br><br>
          <div class="row">
              
       <div class="col-md-12">
           <?php while($room = mysqli_fetch_assoc($room_que_b)) : ?>
        <div class="col-md-2">
            <a href="room_details.php?details=<?=$room['id'] ?>"><button class="big_but btn btn-default btn-block" id="<?=(($room['available'] == 1)? 'btn_free': 'btn_full'); ?>">Room <?=$room['id'] ?></button></a>   
        </div> 
        <?php endwhile ?>
       </div>
      </div>
      
     </div>
     </div> 
</div>

      <?php include 'includes/footer.php' ?>

