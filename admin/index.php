<?php include 'includes/head.php'?>
<?php session_start()?>
<?php
 if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
  header('Location: login.php');
}
?>

<style>
    .big_but{
        padding: 40px;
        border-radius: 10px;
        font-size: 20px;
        margin-top: 10px;
        height: 160px;
        width: 330px
    }
    .col-md-4{
      padding: 5px;
    }

    body{
/*        background-image: image('../images/rm2.jpg')*/
    }
</style>
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
      <div class="container">
      <div class="home">
          <h1 class="text-right">:: Administrator </h1><hr>
      <div class="row">
       <div class="col-md-12">
        <div class="col-md-4">
            <a href="add_room.php"><button class="big_but btn btn-warning btn-block">Add Room <span class="glyphicon glyphicon-plus"></span></button></a>
        </div>
        <div class="col-md-4">
            <a href="E_room.php"><button class="big_but btn btn-primary btn-block">Update Room <span class="glyphicon glyphicon-pencil"></span></button></a>
        </div>
        <div class="col-md-4">
            <a href="available.php"><button class="big_but btn btn-danger btn-block">Available Rooms <span class="glyphicon glyphicon-tag"></span></button></a>
        </div>
       </div>
      </div>
      <div class="clearfix"></div>
        <br><br>
      <div class="row">
       <div class="col-md-12">
        <div class="col-md-4">
            <a href="booked.php"><button class="big_but btn btn-info btn-block">Booked Rooms <span class="glyphicon glyphicon-book"></span></button></a>
        </div>
        <div class="col-md-4">
            <a href="clients.php"><button class="big_but btn btn-success btn-block">Clients Details <span class="glyphicon glyphicon-list"></span></button></a>
        </div>
        <div class="col-md-4">
        <a href="reservation.php"><button class="big_but btn btn-default btn-block">All Reservations <span class="glyphicon glyphicon-edit"></span></button></a>
        </div>
       </div>
      </div>
     </div>
     <div class="clearfix"></div>
        <br><br>
      <div class="row">
       <div class="col-md-12">
        <div class="col-md-4">
        <a href="spa.php"><button class="big_but btn btn-danger btn-block">Spa <span class="glyphicon glyphicon-ice-lolly-tasted"></span></button></a>
        </div>
        <div class="col-md-4">
        <a href="message.php"><button class="big_but btn btn-primary btn-block">View Messages <span class="glyphicon glyphicon-envelope"></span></button></a>
        </div>
        <div class="col-md-4">
        <a href="logout.php"><button class="big_but btn btn-warning btn-block">LogOut <span class="glyphicon glyphicon-lock"></span></button></a>
        </div>
       </div>
      </div>
     </div>

      </div>


</div>

      <?php include 'includes/footer.php' ?>
