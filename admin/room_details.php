
<?php session_start()?>
<?php
 if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
  header('Location: login.php');
}
?>
<?php include 'includes/head.php'?>
<?php include '../core/init.php'?>

<style>
    .big_but{
        padding: 40px;
        border-radius: 10px;
        font-size: 20px;
    }
    
    body{
/*        background-image: image('../images/rm2.jpg')*/
    }
    
    .img{
        height: 500px;
        width: 100%;
    }
    
    .badge{
        color: black;
        background-color: white;
        font-size: 18px;
        font-family: sans-serif;
    }
</style>



<?php if(isset($_GET['details'])){
  $details_id = (int)$_GET['details'];
  $room_img = "SELECT * FROM rooms WHERE id ='$details_id'";
  $room_img_que =$db->query($room_img);
  $room_image = mysqli_fetch_assoc($room_img_que);
    
  $client ="SELECT * FROM checkout WHERE room_number ='$details_id' AND deleted ='0'";
    $client_que =$db->query($client);
    $client_d =mysqli_fetch_assoc($client_que);
}
?>

<?php if(isset($_GET['available'])){
  $avail_id = (int)$_GET['available'];
  $free = "1";
  $free = (int)$free;
    
  $available="UPDATE `rooms` SET `available` = '$free WHERE `id` = $avail_id";
    $db->query($available);
    header('Location :booked.php');
}
?>
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>  
      <div class="container">
      <div class="home">
          <h1 class="text-right">::: <?=(($room_image['available'] == '1')?'Room Currently Available':'Room Currently Booked') ?></h1><hr class="hr">
          <br><br>
          <div class="row">
          <div class="col-md-12">
   <a href="<?=(($room_image['available'] == '1')?'index.php':'rem_avail.php')?>?available=<?= $details_id ?>" class="btn btn-primary pull-right" id="<?=(($room_image['available'] == '1')?'free':'booked') ?>">
    <?=(($room_image['available'] == '1')?'Go Home':'Make Room available') ?>
    </a><br><br><br>
              <p></p>
          <img src="<?=$room_image['image'] ?> " class="img-responsive img-thumbnail img">
          </div>
          </div>
          <br>
          <div class="row">
          <div class="col-md-12">
          <div class="panel panel-primary">
          <div class="panel-heading">Room Details</div>
          <div class="panel-body">
          <section>

    <div class="list-group ">
      <a href="#" class="list-group-item"><span class="badge "><?=$details_id ?></span>Room ID</a>
      <a href="#" class="list-group-item"><span class="badge "><?=$room_image['room_type'] ?></span>Room Type</a>
      <a href="#" class="list-group-item"><span class="badge "><?=$room_image['room_size']?></span>Room Size</a>
      <a href="#" class="list-group-item"><span class="badge badge-success"><?=$client_d['firstname'].' '.$client_d['lastname'] ?></span>Occupant Name</a>
      <a href="#" class="list-group-item"><span class="badge badge-success"><?=$client_d['email'] ?></span>Occupant's Email</a>
      <a href="#" class="list-group-item"><span class="badge badge-success"><?=$client_d['telephone'] ?></span>Telephone</a>
      <a href="#" class="list-group-item"><span class="badge badge-success"><?=$client_d['country'] ?></span>Country</a>
      <a href="" class="list-group-item"><span class="badge "><?=$client_d['city_state']?></span>Occupant City/State</a>
      <a href="" class="list-group-item"><span class="badge "><?=$client_d['address']?></span>Occupant's Address</a>

    </div>

</section>
  
          </div>
          </div>
          </div>
          </div>

      
     </div>
      </div>
      
      
</div>

      <?php include 'includes/footer.php' ?>

