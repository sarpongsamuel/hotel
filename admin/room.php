<?php require_once '../core/init.php' ?>
<?php include 'includes/head.php'?>
<?php include 'save_room.php'?>




<style>
    .btn-warning:hover{
        color: white;
        background-color: red;
    }    
</style>
<?php  
   if(isset($_GET['edit'])){
       $edit_id = $_GET['edit'];
        $rooms = "SELECT * FROM rooms WHERE (deleted ='0' AND id ='$edit_id')";
        $room_que = $db->query($rooms);
       $room =mysqli_fetch_assoc($room_que);
   }  
    
?>
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <span class="logo-lg">hotel deMarciana</span>
    </a>
<?php include 'includes/navbar.php' ?>
  </header>
<?php include 'includes/aside.php'?>
  <div class="content-wrapper">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>  
      <div class="container">
      <div class="home">
          <h1 class="text-right">Update Room  #<?=$room['id'] ?></h1><hr class="hr">
            <div class="row">
                <a href="index.php"><button class="btn btn-md btn-warning pull-right">Cancel Update</button></a>  
                <div class="col-md-12">
                    <br><br>
                 <?=$errors ?>
                 <form method="post" action="">
                 <div class="col-md-6 col-md-offset-2">
                 <label>Room Type*</label>
                     <select class="form-control" name="type">
                      <option value="<?=((isset($room['room_type']))?$room['room_type'] : '') ?>"><?=((isset($room['room_type']))?$room['room_type'] : '') ?></option>
                      <option value="Presidential Suite">Presidential Suite</option>
                      <option value="Junior Suite">Junior Suite</option>
                      <option value="Queen Suite">Queen Suite</option>
                      <option value="Common Room">Common Room</option>
                      <option value="Family Size">Family Size</option>
                     </select> 
                 </div>
                 <div class="col-md-3 col-md-offset-1">
                 <label>Price*</label>
                 <input type="text" size="60" class="form-control" name="price" value="<?=((isset($room['price']))?$room['price'] : '') ?>">    
                 </div>
                 <div class="clearfix"></div> 
                     
                 <div class="col-md-4 col-md-offset-2">
                 <label>Room Size*</label>
                 <input type="text" size="60" class="form-control" name="room_size" value="<?=((isset($room['room_size']))?$room['room_size'] : '') ?>">     
                 </div>
                 <div class="col-md-4 col-md-offset-1">
                 <label>Room Image*</label>
                  <input type="file" class="form-control" name="photo" value="<?=$room['image'] ?>">
                   
                 </div>
                 <div class="clearfix"></div> 
                     
                 <div class="col-md-10 col-md-offset-2">
                 <label>Description*</label>
                 <textarea class="form-control" cols="3" rows="6" name="description" value="<?=((isset($room['description']))?$room['description'] : '') ?>"></textarea>
                 </div>
                 <div class="clearfix"></div> 
                 <br><br><br>
                 <div class="col-md-10 col-md-offset-2">
                 <input type="submit" name="submit" value="Save Changes" class="btn btn-primary form-control">   
                 </div>
                 <div class="clearfix"></div> 
                 </form>
<!--
                    <br>
                 <div class="col-md-10 col-md-offset-2">
                 <a href="index.php"><button class="btn btn-block btn-md btn-warning">Cancel</button></a>  
                 </div>
-->
                </div>
      </div>
     </div>
      
      </div>
      
      
</div>

      <?php include 'includes/footer.php' ?>

