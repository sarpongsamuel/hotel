<?php include 'includes/head.php' ?>
<?php include 'all_query.php' ?>
<?php include 'helpers/helpers.php' ?>
<?php include 'includes/navigate.php' ?>
<style>

.sl_txt{
    font-size: 40px;
    font-family: sans-serif;
/*    border: 1px solid red;*/
    padding: 20px;
    margin-bottom: 40px;
}
    .top{
        color: darkseagreen;
        font-size: 50px;
        font-family: serif;
    }
    .txt{
        color: darkseagreen;
        font-size: 28px;
        font-family: serif;
    }



@media(max-width:768px){
    .img{
        height: 300px;
        width: 100%;
        margin-top: 10px;
    }

    #room{
  height: 400px;
    width: 100%;

}

    .carousel-inner > .item{
  height: 300px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 20%;
}
    .banner{
        font-size: 20px;
        margin-top: -20px;
    }
  }


@media(max-width:1028px){
    .myimg{
        height: 300px;
        width: 100%;
    }

    .carousel-inner > .item{
  height: 600px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 20px;
  left: 0;
  min-width: 100%;
  height: 100%;
}
    .banner{
        font-size: 20px;
        margin-top: -10px;
    }
  }

/*    iphone X*/
    @media(max-width:375px){
    #room{
        height: 220px;
        width: 100%;
    }
        .top{
            font-size: 30px;
        }
        .txt{
            font-size: 20px;
            font-family: serif;
        }
        .desc{
            font-size: 17px;
            font-family: serif;
        }

        .amt{
            font-size: 17px;
            font-family: serif;
        }
        .toptxt{
            font-size: 30px;
        }

    .carousel-inner > .item{
  height: 410px;
}
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
  }

    /*    iphone 6/7 */
    @media(max-width:414px){
    #room{
        height: 230px;
        width: 100%;
    }

        .top{
            font-size: 30px;
            color: darkseagreen;
        }
        .txt{
            font-size: 20px;
            padding: 5px;
            text-align: center
        }
        .desc{
            font-size: 20px;
            font-family: serif;
        }
        .amt{
            font-size: 20px;
            font-family: serif;
        }

    .carousel-inner > .item{
       height: 410px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 30px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 20px;
        }
  }

    .carousel-inner > .item{
  height: 590px;
}

        @media(max-width:1126px){
    .carousel-inner > .item{
       height: 500px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 50px;
/*        margin-top: -10px;*/
        padding: 25px;
    }
        .carousel-caption{
            font-size: 30px;
        }
  }

    @media(max-width:360px){
    .carousel-inner > .item{
       height: 410px;
    }
    .carousel-inner > .item >img{
  position: relative;
  top: 10px;
  left: 0;
  min-width: 100%;
  height: 100%;
}

    .banner{
        font-size: 15px;
/*        margin-top: -10px;*/
        padding: 20px;
    }
        .carousel-caption{
            font-size: 20px;
        }
  }
</style>

 <div id="myCarousel" class="carousel slide">
   <div class="carousel-inner">
     <div class="item active">
       <img src="images/slider/fm9.jpg">
         <div class="container-active">
       <div class="carousel-caption">
         <h1>HOTEL DEMARCIANA</h1>
         <p>Book directly with us</p>
       </div>
     </div>
   </div>
   <div class="item">
     <img src="images/slider/Body-Wrap.jpg">
       <div class="container-active">
     <div class="carousel-caption">
       <h1>HOTEL DEMARCIANA</h1>
       <p>Best Available Rate Guarantee assures you receive the best rates when you book directly with us</p>
<!--      <p> <a href="#" class="btn btn-primary">Sign up today</a></p>-->
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/slider/s9.jpg">
       <div class="container-active">
     <div class="carousel-caption">
       <h1>HOTEL DEMARCIANA</h1>
       <p>Best Available Rate Guarantee assures you receive the best rates when you book directly with us</p>
<!--      <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>-->
     </div>
   </div>
 </div>
 <div class="item">
     <img src="images/slider/gym4.jpg">
       <div class="container-active" class="fade">
     <div class="carousel-caption">
        <h1>HOTEL DEMARCIANA</h1>
       <p>Best Available Rate Guarantee assures you receive the best rates when you book directly with us</p>
<!--      <p> <a href="#" class="btn btn-primary">Sign up today</a></p>-->
     </div>
   </div>
 </div>
 <div class="item ">
   <img src="images/slider/s444.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1>HOTEL DEMARCIANA</h1>
     <p>Best Available Rate Guarantee assures you receive the best rates when you book directly with us</p>
<!--    <p> <a href="#" class="btn btn-primary-large">Sign up today</a></p>-->
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/slider/bed7.jpg">
     <div class="container-active">
   <div class="carousel-caption">
     <h1>HOTEL DEMARCIANA</h1>
     <p>  Best Available Rate Guarantee assures you receive the best rates when you book directly with us   </p>
<!--    <p> <a href="#" class="btn btn-success">Sign up today</a></p>-->
   </div>
 </div>
</div>
<div class="item ">
   <img src="images/slide1%20(20).jpg">
     <div class="container-active">
   <div class="carousel-caption">
      <h1>HOTEL DeMARCIANA</h1>
       <p>Best Available Rate Guarantee assures you receive the best rates when you book directly with us</p>
<!--    <p> <a href="#" class="btn btn-success">Sign up today</a></p>-->
   </div>
 </div>
</div>
 </div>
<!-- <a href="#myCarousel" class="left carousel-control" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
<a href="#myCarousel" class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
 <!-- end of carousel -->
</div>
<!--banner here-->
<?php include 'includes/banner.php';?>

<!--first content on page-->
<div class="container-padded">
<div class="rooms_gal">
<div class="row">
    <div class="col-md-12">

    <div class="col-md-9">
    <div class="row">
      <h2 class="text-center top">ROOMS AVAILABLE</h2>
      <hr>
      <?php while($rooms=mysqli_fetch_assoc($room_que)) :?>
      <div class="col-md-3">
        <h3 class="txt"><?php echo $rooms['room_type']; ?></h3>
        <img src="<?php echo $rooms['image']; ?>" alt="<?php echo $rooms['room_type']; ?>" class="img-responsive img-thumbnail" id="room">
        <br>
          <p></p>
             <br>
              <ul>
                <li class="desc">Description : <?=$rooms['description'] ?><span></span></li>
                <li class="amt">Amount : <?=currency($rooms['price']) ?><span></span></li>
              </ul>
        <a href="checkout.php?book=<?=$rooms['id'] ?>" class="btn btn-success form-control">Book Now</a>
        </div>
      <?php endwhile; ?>
      </div>
    </div>

<?php include 'includes/leftbar.php'?>

<!--second row of images-->
</div>
</div>
</div>
</div>

  <?php include 'includes/footer.php' ?>
